# -*- coding: utf-8 -*-
"""
Generate the quantization example.
"""
__author__ = "Gerald Ebberink"
__copyright__ = "Copyright 2016, Gerald Ebberink"
__credits__ = ["Gerald Ebberink"]
__license__ = "GPL v2"
__version__ = "1.0.0"
__maintainer__ = "Gerald Ebberink"
__email__ = "g.h.p.ebberink@saxion.nl"
__status__ = "Production"

import numpy
import matplotlib.pyplot as plt

x = numpy.arange(0, 10, 0.01)
y = 3 + 3 * numpy.sin(3 * x)
y2 = y // 1
plt.figure(figsize=(16,12), dpi=25)
plt.plot(x,y, linewidth=2.0, label='Real Signal')
plt.plot(x,y2, linewidth=2.0, label='Quantized Signal')

plt.ylim(0,6.5)
plt.legend()
plt.title('Quantization')
plt.xlabel('Time[s]')
plt.ylabel('Amplitude [au]')

