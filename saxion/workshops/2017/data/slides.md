class: centre, middle, inverse
# Introduction in data
Master Nanotechnology
.left[2017-08-28]
.footnote[Gerald Ebberink]

---

name: TOC
class: middle
.left-column[
## Table of contents
]
.right-column[
1. Introduction
2. What is data
3. Different types of data
4. Acquisition
6. Analysis
7. General Recommendations
]
???

# Just go forward, the real intro is coming.

---
class: top
layout: true
---

.left-column[
## Introduction
### who am I?
]
.right-column[
Name: Gerald Ebberink<br />
Studies:
* BSc in Applied Physics
* BSc in Electronics

Work:<br />
Working in research and development for 15 years<br />
Different fields, from Nuclear Physics and laser technology
to diffractive optics and now .red[**Nanotechnology**]

Family:<br />
* Partner
* Twin boys

Hobbies:<br />
* Shooting Sports
* Battling Robots
* Swimming
* Tinkering
]

???

# Don't stay here to long

---
.left-column[
## Introduction
### who am I?
### what to expect
]
.right-column[
* Short introduction to data
]

???

# Brief / Short.

---
count: false

.left-column[
## Introduction
### who am I?
### what to expect
]
.right-column[
* Short introduction to data
* Not complete
]

???

# Note that not some parts are complete studies or programmes

---
count: false

.left-column[
## Introduction
### who am I?
### what to expect
]
.right-column[
* Short introduction to data
* Not complete
* Able to recognize data types
* Ideas what you can do with data
]

---
.left-column[
## What is data
### Definition
]
.right-column[
[Cambridge Dictionary][1]:<br />
> **information**, especially facts or numbers,<br />
> **collected** to be **examined** and considered and used <br />
> to help **decision-making**.red[* ]<br /> 
]

.footnote[.red[* ]emphasis mine]

---
count: false

.left-column[
## What is data
### Definition
]
.right-column[
[Cambridge Dictionary][1]:<br />
> **information**, especially facts or numbers,<br />
> **collected** to be **examined** and considered and used <br />
> to help **decision-making**.red[* ]<br />

- **Information:** In technology heavy research and development
(like .red[nanotechnology]), these are numbers.
- **collected:** "Meten is weten." This is done in either a digital (computer),
  or analog (lab-book) format.
- **examined:** To be able to examine the data, it has to be presented to the
  user.
- **decision-making:** This part is important, collecting numbers just for
  collecting them is a wast of time.
]

.footnote[.red[* ]emphasis mine]

???
# Meten is weten: To know is to measure.

---
.left-column[
## Different types of data
]
.right-column[
Data can be grouped in several ways:
- By dimension
- By numerical representation
- By source
]

---
count: false
.left-column[
## Different types of data
]
.right-column[
Data can be grouped in several ways:
- By dimension .red[Let us get started!]
- By numerical representation
- By source
]

---
.left-column[
## Different types of data
### zero dimensions
]
.right-column[
# A Number
.center[![Powermeter][2]

*A powermeter in a femtosecond laser setup* ]
- Single number
- No relation to other values (not even time)
- Provide context with your notes (labbook).<br />
  To help you with later decision making.
- Very common: outside temperature, value on a multimeter or the average power
  of a femtosecond laser.
]
---
.left-column[
## Different types of data
### zero dimensions
### one dimension
]
.right-column[
# A Vector
.center[![Scatter plot][3]

*Series of numbers*]

- A series of numbers
- Show progress of a single quantity
- Still no relation to other values (not even time)
]
---
.left-column[
## Different types of data
### zero dimensions
### one dimension
### 2 dimensions
]
.right-column[
# Two Vectors
- Each data point has two values
- Relationship between two quantities
- Different types of data content
  - Series
  - Spectra
]
---
.left-column[
## Different types of data
### zero dimensions
### one dimension
### 2 dimensions
#### Series
]
.right-column[
# Series
.center[![Series][4]

*Plot of a laser setting in percent to the output power in Watt*]
- Relationship between two quantities
- Horizontal the quantity which is deliberately changed
- Vertical the investigated quantity
- Important information in the trend
]
---
.left-column[
## Different types of data
### zero dimensions
### one dimension
### 2 dimensions
#### Series
#### Spectrum
]
.right-column[
# Spectrum
.center[![Spectrum][5]

*Spectrum of resonance frequencies<br />
 (from the Triup project)*]
- Specific case of a Series
- Information in position and shape of the peaks
- Not so much in the height or the trend
]
---
.left-column[
## Different types of data
### zero dimensions
### one dimension
### 2 dimensions
### 3 dimensions
]
.right-column[
# Three Vectors?
Sometimes.
- Each data point now has three values associated
- Width and Depth deliberately changed
- Important information in the trend
]
---
.left-column[
## Different types of data
### zero dimensions
### one dimension
### 2 dimensions
### 3 dimensions
#### 3D series
]
.right-column[
# Three Vectors?
Yes!
.center[
![3D Series][6]

*example of a 3D scatter plot*  
]
- Each data point now has three values associated
- Width and Depth deliberately changed
- Important information in the trend
]
---
.left-column[
## Different types of data
### zero dimensions
### one dimension
### 2 dimensions
### 3 dimensions
#### 3D series
#### Image
]
.right-column[
# Three Vectors?
No!
.center[
![Image][7]

*example of an image plot*
]
- Width and Depth each spaced equally
- Two Vectors and a matrix
- Third dimension is represented by color
]
---
.left-column[
## Different types of data
### zero dimensions
### one dimension
### 2 dimensions
### 3 dimensions
#### 3D series
#### image
#### contour plot
]
.right-column[
# Three Vectors?
No!
.center[
![Contour][8]

*example of a contour plot*
]
- When interest is in the position of a specific value
- Think about altitudes, isobars, isochores
- certain values of the third dimension are represented by lines  
]
---
.left-column[
## Different types of data
### zero dimensions
### one dimension
### 2 dimensions
### 3 dimensions
#### image
#### contour plot
#### surface projection
]
.right-column[
# Three Vectors?
No!
.center[
![surface projection][9]

*example of a surface projection*
]
- often called 3D view
- fixed intervals for Width and Depth
- Two Vectors and a matrix
]
---
.left-column[
## Data Acquisition
]
.right-column[
- data acquisition is process of gathering data.
- analog in lab book
- digital in a computer
]
---
.left-column[
## Data Acquisition
### sampling
]
.right-column[
.center[![Sampling][10]]
- From continues signal to discreet samples
- Sample rate or sample frequency f<sub>s</sub>
- f<sub>s</sub> = 1/T
]

???
# Explain about sample time.
---
.left-column[
## Data Acquisition
### sampling
### quantization
]
.right-column[
.center[![Sampling][11]]
- ADC's have only specific voltage levels
- all values around this are translated to that value
- This gives steps instead of smooth lines
]
???

# If the steps are small enough you will not notice
---
.left-column[
## Data Acquisition
### pitfalls
]
.right-column[
Analog pitfalls:
- Observer dependency
  - Parallax errors (mercury thermometer)
  - Estimate errors (Litmus test)
  - Meniscus errors (measuring flask)

Digital pitfalls:
- Digital errors
  - Aliasing
  - Quantization
  - Noise
]
---
.left-column[
## Data Acquisition
### pitfalls
]
.right-column[
Analog pitfalls:
- Observer dependency
  - Parallax errors (mercury thermometer)
  - Estimate errors (Litmus test)
  - Meniscus errors (measuring flask)

Digital pitfalls: .red[Next slides]
- Digital errors
  - Aliasing
  - Quantization
  - Noise
]
---
.left-column[
## Data Acquisition
### pitfalls
#### aliasing
]
.right-column[
.center[![Aliasing][12]

_Example of aliasing.red[*]_
]
Problem:
- sampling at low sample frequency a high speed signal
- non existing signals shown
- possibly wrong decisions made

Solution:
- For periodic signals have a sample frequency which is at least twice as high as the highest signal
  frequency.
- lower signal speed (anti aliasing filter)
- increase sampling frequency<br />
Nyquist rate (f<sub>N</sub>=2f<sub>0</sub>)
]
.footnote[.red[* ][By Moxfyre - Own work, CC BY-SA 3.0](https://commons.wikimedia.org/w/index.php?curid=6596059)]

---
.left-column[
## Data Acquisition
### pitfalls
#### aliasing
]
.right-column[
# Illustrative example
.center[
<iframe width="400" height="255" src="https://www.youtube.com/embed/i8sad03JAP0?t=0m26s" frameborder="0" allowfullscreen></iframe>]
]
---
count: false
.left-column[
## Data Acquisition
### pitfalls
#### aliasing
]
.right-column[
# Illustrative example
.center[
<iframe width="400" height="255" src="https://www.youtube.com/embed/i8sad03JAP0?t=0m26s" frameborder="0" allowfullscreen></iframe>]

- many droplets
- each flash of the strobe is a sample
- in the dark the camera (or your eye) do not register so no sample
]
---
.left-column[
## Data Acquisition
### pitfalls
#### aliasing
]
.right-column[
# Illustrative example 2
.center[
<iframe width="400" height="300" src="https://www.youtube-nocookie.com/embed/qgvuQGY946g?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>]
- camera frequency equal to  rotor frequency / 5
- every new image, the rotors are in the "next" position
]
---
.left-column[
## Data Acquisition
### pitfalls
#### aliasing
#### quantization
]
.right-column[
.center[![Quantization][13]]
Problem:
- Quantization gives you steps
- After smoothing you can get a different signal shape
Solution:
- Increase number of used levels
  - Better matching of input signal to ADC
  - Use ADC with better specs
]

???

# Pre amplification
# different voltage range
---
.left-column[
## Data Acquisition
### pitfalls
#### aliasing
#### quantization
#### noise
]
.right-column[
.center[
![Noise][14]

_Example of noise on a signal.red[* ]_
]
Problem:
- signal not or badly visible
- bad decision

Solution:
- increase SNR (Signal to Noise ratio)
  - increase signal levels if possible
  - longer integration times (e.g. measure longer)
  - filtering
- remove noise sources
]
.footnote[.red[* ][By Christophe Dang Ngoc Chan (cdang) - Own work, CC BY-SA 3.0](https://commons.wikimedia.org/w/index.php?curid=1137149)]
---
.left-column[
## Data Acquisition
### pitfalls
#### aliasing
#### quantization
#### noise
]
.right-column[
# Example of noise in art
.center[
<iframe width="400" height="300" src="https://www.youtube-nocookie.com/embed/6bN14upNp6U?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>]
- Not only bad
  - [noise][simplyrain] helps concentrating
  - noise can characterize your setup
]
???
#Artist I once met
---
.left-column[
## Analysis
]
.right-column[
# Analysis of the data
- prepare your data for decision making
  - measuring smart
  - cleaning of data
  - presenting large data sets
]
---
.left-column[
## Analysis
### measuring smart
]
.right-column[
.center[![CMC-example][15]]
- establish a baseline
- Calibrate -> Measure > Calibrate
- background subtraction
]
---
.left-column[
## Analysis
### measuring smart
### data cleanup
]
.right-column[
.center[![Fourier_filtering][16]]
- remove strange incorrect values from data by hand
  - Note this in laboratory notebook
- start with a moving average
- for periodic disturbances use Fourier filtering
]
---
.left-column[
## Analysis
### measuring smart
### data cleanup
### Large datasets
]
.right-column[
.center[![Boxplot][17]]
]
???
# Low number of sample points
# Many > 10 measurements per position
---
.left-column[
## Analysis
### measuring smart
### data cleanup
### Large datasets
]
.right-column[
.center[![Error bars][18]]
]
???
# Point out error bars,
# Scatter plots etc.
---

# General Recommendations

--
- Always use a **COPY** of your data
--

- Always store the scripts or software with your images
--

- Backup data following the 3-2-1 rule
--

- Use software for Analysis
  - Matlab
  - Python
  - Specialized software
---
class: center, middle
#QUESTIONS?

[simplyrain]: https://rain.today/ "rain noise generator"
[reader]: http://geraldebberink.bitbucket.org/saxion/workshops/2016/data/reader.pdf "Reader"
[1]: http://dictionary.cambridge.org/dictionary/english/data "Definiton according to the Cambride Dictionary"
[2]: assets/wp_20131007_002.jpg "A powermeter in a femtosecond laser setup"
[3]: assets/1D.png "Series of random numbers"
[4]: assets/series-example.png "Plot of a laser setting in percent to the output power in Watt"
[5]: assets/spectrum-example.png "Spectrum of resonance frequencies (from the Triup project)"
[6]: assets/3dline-example.png "example of a 3D scatter plot"
[7]: assets/image-example.png "example of an image plot"
[8]: assets/contour-example.png "example of a contour plot"
[9]: assets/surface-example.png "example of a surface projection"
[10]: assets/Signal_Sampling.png "example of sampling"
[11]: assets/quantization-example.png "example of quantization"
[12]: assets/AliasingSines.svg "example of aliasing"
[13]: assets/quantization-noise-example.png "example of quantization noise"
[14]: assets/noise-example.png "example of noise"
[15]: assets/abs_shift.jpg "example of C M C"
[16]: assets/Fourier_filtering.png "example of 2D Fourier_filtering"
[17]: assets/Michelsonmorley-boxplot.svg "example of boxplot"
[18]: assets/error-bars.gif "example error-bars"
